import Vue from 'vue';
import VueRouter from 'vue-router';
import homework from '../components/homework.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'homework',
    component: homework,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
