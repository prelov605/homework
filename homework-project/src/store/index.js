import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    mainModal: false,
    cards: [],
  },
  mutations: {
    CHANGE_MAIN_MODAL: (state) => {
      state.mainModal = !state.mainModal;
    },
    PUSH_IN_CARDS: (state, payload) => {
      state.cards.push(payload);
    },
    SPLICE_FROM_CARDS: (state, payload) => {
      state.cards.splice(payload, 1);
    },
    SORT_CARDS: (state, payload) => {
      console.log(payload);
      if (payload === 'Sort by Invoice number: ascending') {
        const sortArr = state.cards.sort((a, b) => (+a.message > +b.message ? 1 : -1));
        console.log(sortArr);
      } else {
        const sortArr = state.cards.sort((a, b) => (+b.message > +a.message ? 1 : -1));
        console.log(sortArr);
      }
    },
    FILTER_CARDS: (state, payload) => {
      state.cards.forEach((card) => {
        // eslint-disable-next-line no-param-reassign
        card.display = card.message.includes(payload);
        if (payload === '') {
          // eslint-disable-next-line no-unused-expressions
          card.display === true;
        }
      });
      console.log(payload);
    },
  },
  actions: {
    CHANGE_MAIN_MODAL(context) {
      context.commit('CHANGE_MAIN_MODAL');
    },
    PUSH_IN_CARDS: (context, payload) => {
      context.commit('PUSH_IN_CARDS', payload);
    },
    SPLICE_FROM_CARDS: (context, payload) => {
      context.commit('SPLICE_FROM_CARDS', payload);
    },
    FILTER_CARDS: (context, payload) => {
      context.commit('FILTER_CARDS', payload);
    },
    SORT_CARDS: (context, payload) => {
      context.commit('SORT_CARDS', payload);
    },
  },
  getters: {
  },
  modules: {
  },
});
